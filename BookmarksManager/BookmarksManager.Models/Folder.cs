﻿using System;
using System.Collections.Generic;

namespace BookmarksManager.Models
{
    public class Folder
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Folder Parent { get; set; }
        public List<Bookmark> Bookmarks { get; set; } = new List<Bookmark>();
        public List<Folder> Folders { get; set; } = new List<Folder>();
    }
}
