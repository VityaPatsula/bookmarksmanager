﻿using System;
using System.Collections.Generic;

namespace BookmarksManager.Models
{
    public class Bookmark
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public Uri Link { get; set; }
        public string Description { get; set; }
        public Folder Directory { get; set; }
        public List<Tag> Tags { get; set; }
    }
}
