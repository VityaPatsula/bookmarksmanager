﻿using BookmarksManager.Infrastructure.Managers.Models.Chrome;
using BookmarksManager.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace BookmarksManager.Infrastructure.Managers
{
    public class ChromeManager
    {
        public async Task<List<Folder>> ImportFromJson(Stream stream)
        {
            var root = await JsonSerializer.DeserializeAsync<Root>(stream);

            var folders = new List<Folder>();
            foreach (var node in root.Roots.Nodes)
            {
                var folder = new Folder()
                {
                    Name = node.Name,
                    Parent = null
                };
                MapFolder(folder, node);
                folders.Add(folder);
            }
            return folders;
        }

        private Bookmark MapBookmark(Folder parent, Node node)
        {
            return new Bookmark()
            {
                Link = new Uri(node.Url),
                Description = string.Empty,
                Title = node.Name,
                Directory = parent
            };
        }

        private void MapFolder(Folder container, Node node)
        {
            container.Bookmarks = node.Children.Where(x => x.Type == NodeType.Url).Select(x => MapBookmark(container, x)).ToList();

            var folders = node.Children.Where(x => x.Type == NodeType.Folder).ToList();
            foreach (var f in folders)
            {
                var folder = new Folder()
                {
                    Name = f.Name,
                    Parent = container
                };

                MapFolder(folder, f);
                container.Folders.Add(folder);
            }
        }
    }
}
