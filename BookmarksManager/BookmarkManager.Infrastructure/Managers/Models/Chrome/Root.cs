﻿using System.Text.Json.Serialization;

namespace BookmarksManager.Infrastructure.Managers.Models.Chrome
{
    public class Root
    {
        //[JsonIgnore]
        //[JsonPropertyName("checksum")]
        //public string Checksum { get; set; }

        [JsonPropertyName("roots")]
        public Roots Roots { get; set; }

        //[JsonIgnore]
        //[JsonPropertyName("sync_metadata")]
        //public string SyncMetadata { get; set; }

        //[JsonIgnore]
        //[JsonPropertyName("version")]
        //public int Version { get; set; }
    }
}
