﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace BookmarksManager.Infrastructure.Managers.Models.Chrome
{
    public class Roots
    {
        [JsonPropertyName("bookmark_bar")]
        public Node BookmarkBar { get; set; }

        [JsonPropertyName("other")]
        public Node Other { get; set; }

        [JsonPropertyName("synced")]
        public Node Synced { get; set; }

        public List<Node> Nodes
        {
            get
            {
                return new List<Node>() { BookmarkBar, Other, Synced };
            }
        }
    }
}
