﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace BookmarksManager.Infrastructure.Managers.Models.Chrome
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum NodeType
    {
        [EnumMember(Value = "folder")]
        Folder,
        [EnumMember(Value = "url")]
        Url
    }
    public class Node
    {
        [JsonPropertyName("children")]
        public List<Node> Children { get; set; }

        [JsonPropertyName("date_added")]
        public string DateAdded { get; set; }

        //[JsonPropertyName("date_modified")]
        //public string DateModified { get; set; }

        //[JsonPropertyName("guid")]
        //public Guid Guid { get; set; }

        //[JsonPropertyName("id")]
        //public string Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("type")]
        public NodeType Type { get; set; }

        [JsonPropertyName("url")]
        public string Url { get; set; }
    }
}
